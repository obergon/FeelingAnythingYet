﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingManager : MonoBehaviour
{
	public List<Light> spotLights = new List<Light>();
	public List<Color> lightColours = new List<Color>();

	public float minLightTime = 0.1f;
	public float maxLightTime = 0.5f;

	private bool isChill;
	private bool lightsOn;
		
		
	private void Start()
	{
		AllLightsOff();
	}

    private void OnEnable()
	{
		TripEvents.OnMusicBeat += OnBeat;
		TripEvents.OnMusicHalfBeat += OnHalfBeat;
        TripEvents.OnMusicMoodChange += OnMusicMoodChange;
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnTripRestart += OnTripRestart;
        TripEvents.OnSpotLights += OnSpotLights;
	}

	private void OnDisable()
	{
		TripEvents.OnMusicBeat -= OnBeat;
		TripEvents.OnMusicHalfBeat -= OnHalfBeat;
        TripEvents.OnMusicMoodChange -= OnMusicMoodChange;
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnTripRestart -= OnTripRestart;
        TripEvents.OnSpotLights -= OnSpotLights;
    }



	private void ActivateLights(int numLights)
	{
		if (!lightsOn)
			return;
			
		for (int i = 0; i < numLights; i++)
		{
			StartCoroutine(ActivateRandomLight(UnityEngine.Random.Range(minLightTime, maxLightTime)));
		}
	}

	private IEnumerator ActivateRandomLight(float lightTime)
	{
		if (!lightsOn)
			yield break;
			
		var spotLight = spotLights[UnityEngine.Random.Range(0, spotLights.Count)];
		spotLight.color = lightColours[UnityEngine.Random.Range(0, lightColours.Count)];

		spotLight.gameObject.SetActive(true);
		yield return new WaitForSeconds(lightTime);
		spotLight.gameObject.SetActive(false);
		yield return null;
	}
	
	private void AllLightsOff()
	{
		foreach (var spotLight in spotLights)
		{
			spotLight.gameObject.SetActive(false);
		}
	}
    
    private void OnSpotLights(bool on)
	{
		lightsOn = on;
	}
    
    private void OnTripRestart()
	{
		AllLightsOff();
	}

	private void OnTripStart(bool chill)
	{
		isChill = chill;
	}

	private void OnMusicMoodChange(BeatConductor.MusicMood mood)
	{
		switch (mood)
		{
			case BeatConductor.MusicMood.Normal:
				break;
			case BeatConductor.MusicMood.Intense:
				break;
			case BeatConductor.MusicMood.Quiet:
				break;
		}
	}

	private void OnBeat(float barInterval, int beatsPerBar, int beatCount, float curveValue)
	{

	}
	
	private void OnHalfBeat(float barInterval, int beatsPerBar, int beatCount, float curveValue)
	{

	}
}
