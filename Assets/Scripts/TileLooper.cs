﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileLooper : MonoBehaviour
{
	public GroundTile tilePrefab;
	public float groundSpeed;

    public float normalMoodSpeedFactor;
    public float manicMoodSpeedFactor;
    public float quietMoodSpeedFactor;
    public float outroSpeedFactor;
    
    public float anxiousSpeedFactor = 0.5f;

    private int tilesDeep = 16;
	private int tilesWide = 1;
	private float tileSpacingZ = 200f;

	private bool groundScrolling = false;
    private float moodSpeed;
	private int moveTweenId = 0;

	private bool chillTrip = false;
	private Vector3 startPosition;

	private Coroutine moveGroundCoroutine = null;

	private Queue<GroundTile> groundTiles = new Queue<GroundTile>();

	private void Start()
	{
		startPosition = transform.position;
		Init();
	}

	private void OnEnable()
	{
		TripEvents.OnTripStart += OnTripStart;
		//TripEvents.OnBeatStarted += OnBeatStarted;
		TripEvents.OnMusicBass += OnBass;
        TripEvents.OnMusicMoodChange += OnMusicMoodChange;    
        TripEvents.OnMusicOutro += OnMusicOutro;    
        TripEvents.OnTripRestart += OnRestart;    
	}

	private void OnDisable()
	{
		TripEvents.OnTripStart -= OnTripStart;
		//TripEvents.OnBeatStarted -= OnBeatStarted;
		TripEvents.OnMusicBass -= OnBass;
        TripEvents.OnMusicMoodChange -= OnMusicMoodChange;
        TripEvents.OnMusicOutro -= OnMusicOutro;
        TripEvents.OnTripRestart -= OnRestart;        
    }

	private float TripSpeedFactor { get { return chillTrip ? 1.0f : anxiousSpeedFactor;  }}


	private void Init()
	{
		BuildGroundTiles();
		groundScrolling = false;
        moodSpeed = groundSpeed * normalMoodSpeedFactor;
	}

	private void OnTripStart(bool isChill)
	{
		chillTrip = isChill;
	}

	//private void OnBeatStarted(float barInterval, int beatsPerBar)
	//{
	//	StopGround();
	//	moveGroundCoroutine = StartCoroutine(MoveGround());
	//}
	
	
	private void OnBass()
	{
		StopGround();
		moveGroundCoroutine = StartCoroutine(MoveGround());
	}
	
	private void OnRestart()
	{
        Debug.Log("TileLooper.OnRestart");
		StopGround();

		foreach (var tile in groundTiles)
		{
			tile.ClearSpawned();
			Destroy(tile.gameObject);
		}

		groundTiles.Clear();
		
		transform.position = startPosition;
		Init();
	}

    private void OnMusicMoodChange(BeatConductor.MusicMood mood)
    {
		if (!groundScrolling)
			return;
			
        switch (mood)
        {
            case BeatConductor.MusicMood.Normal:
				moodSpeed = groundSpeed * normalMoodSpeedFactor * TripSpeedFactor; 
				UpdateMoveSpeed();
                break;

            case BeatConductor.MusicMood.Quiet:
                moodSpeed = groundSpeed * quietMoodSpeedFactor * TripSpeedFactor; ;
				UpdateMoveSpeed();
                break;

            case BeatConductor.MusicMood.Intense:
                moodSpeed = groundSpeed * manicMoodSpeedFactor * TripSpeedFactor; ;
				UpdateMoveSpeed();
                break;
        }
    }

    private IEnumerator MoveGround()
	{	
		if (groundScrolling)
			yield break;

		groundScrolling = true;

		while (groundScrolling)
		{
			//Debug.Log("MoveGround");
			Vector3 targetPosition = new Vector3(transform.position.x, transform.position.y,
															transform.position.z - tileSpacingZ);
			moveTweenId = LeanTween.move(gameObject, targetPosition, moodSpeed)
									.setEase(LeanTweenType.linear)
									.setOnComplete(() =>
									{
										FirstTileToEnd();		// move to end of queue
									}).id;

			yield return new WaitForSeconds(moodSpeed);
		}
		
		yield return null;
	}

	private void StopGround()
	{
		groundScrolling = false;
			
		if (moveGroundCoroutine != null)
		{
		    Debug.Log("TileLooper.StopGround");	
			StopCoroutine(moveGroundCoroutine);
			moveGroundCoroutine = null;
		}

		if (moveTweenId != 0)
		{
			LeanTween.cancel(moveTweenId);
			moveTweenId = 0;
		}
	}

	// TODO: reinstate?
	private void UpdateMoveSpeed()
	{
		//LTDescr tween = LeanTween.descr(moveTweenId);
		//tween.setTime(moodSpeed);
	}
	
	
	private void OnMusicOutro()
	{
		//groundScrolling = false;
		
	    moodSpeed = groundSpeed * outroSpeedFactor;
		UpdateMoveSpeed();
	}

	private void FirstTileToEnd()
	{
		var firstTile = groundTiles.Peek();
		var tilePosition = firstTile.transform.position;

		firstTile.ClearSpawned();
		firstTile.transform.position = new Vector3(tilePosition.x, tilePosition.y,
															tilePosition.z + (tileSpacingZ * groundTiles.Count));

		groundTiles.Dequeue();
		groundTiles.Enqueue(firstTile);
	}
 
 	private void BuildGroundTiles()
	{
		GroundTile newTile;
		Vector3 buildStartPosition = transform.position;

		Debug.Log("BuildGrooundTiles");
		for (int x = 0; x < tilesWide; x++)
		{
			//for (int z = 0; z < tilesDeep; z++)
			for (int z = -(tilesDeep/2); z < (tilesDeep/2); z++)
			{
				newTile = Instantiate(tilePrefab, this.transform);
				newTile.transform.localPosition = new Vector3(buildStartPosition.x + (tileSpacingZ * x),
													buildStartPosition.y, buildStartPosition.z + (tileSpacingZ * z));

				groundTiles.Enqueue(newTile);
			}
		}
	}
}
