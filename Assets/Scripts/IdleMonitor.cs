﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class IdleMonitor : MonoBehaviour
{
	public float inactivityTime;           		// switch to idle after inactivity seconds
	private DateTime lastActivityTime;
	
	public float idleTime;           		// cut to another idle camera after idleTime seconds
	private DateTime lastIdleCheck;
	
	private bool isIdle = false;
	private bool tripStarted = false; 
	private bool inOutro = false; 


	private void OnEnable()
    {
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnTripRestart += OnTripRestart;
        TripEvents.OnPlayerInput += OnPlayerInput;
        TripEvents.OnMusicOutro += OnMusicOutro;
	}
	
	private void OnDisable()
    {
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnTripRestart -= OnTripRestart;
        TripEvents.OnPlayerInput -= OnPlayerInput;
        TripEvents.OnMusicOutro -= OnMusicOutro;
	}


	private void Update()
	{
		if (tripStarted && ! inOutro)
			CheckInactivity();
	}


	private void OnTripStart(bool isChill)
	{
		lastActivityTime = DateTime.Now;
		tripStarted = true;
		inOutro = false;
	}
	
	private void OnMusicOutro()
	{
		inOutro = true;
	}

	private void OnPlayerInput()
	{
		lastActivityTime = DateTime.Now;
		lastIdleCheck = DateTime.Now;

		if (isIdle)
		{
			isIdle = false;
			TripEvents.OnPlayerIdle?.Invoke(false, false);
		}
	}
	
	private void OnTripRestart()
	{
		isIdle = false;
		inOutro = false;
		tripStarted = false;
		
		lastActivityTime = DateTime.Now;
		lastIdleCheck = DateTime.Now;
	}
	
	
	private void CheckInactivity()
	{
		if (!isIdle)
		{
			if ((DateTime.Now - lastActivityTime).TotalMilliseconds > (inactivityTime * 1000f))	// inactivityTime is in milliseconds
			{
				isIdle = true;
				TripEvents.OnPlayerIdle?.Invoke(true, false);
				
				lastIdleCheck = DateTime.Now;
			}
		}
		else 		// check for prolonged idle time (eg. to switch camera)
		{
			if ((DateTime.Now - lastIdleCheck).TotalMilliseconds > (idleTime * 1000f))		// idleTime is in milliseconds
			{
				TripEvents.OnPlayerIdle?.Invoke(true, true);
				lastIdleCheck = DateTime.Now;
			}
		}
	}
	
	//private void CheckInactivity()
	//{
	//	// inactivityTime is in milliseconds
	//	if ((DateTime.Now - lastActivityTime).TotalMilliseconds > (inactivityTime * 1000f))
	//	{
	//		if (!isIdle)
	//		{
	//			isIdle = true;
	//			TripEvents.OnPlayerIdle?.Invoke(true);
	//		}
	//	}
	//}

}
