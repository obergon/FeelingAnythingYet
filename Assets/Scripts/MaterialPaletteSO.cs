﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Material Palette")]
public class MaterialPaletteSO : ScriptableObject
{
    public Material[] materialGroup;
    
    public Material RandomMaterial { get { return materialGroup [ Random.Range(0, materialGroup.Length) ]; } }
}
