﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaylightChanger : MonoBehaviour
{
	public Light sun;			// main directional light
	public Light moon;	
	
	public float startSkyRotateTime;
	public float chillSkyRotateTime;
	public float anxietySkyRotateTime;
	
	public float startLightRotateTime;
	public float chillLightRotateTime;
	public float anxietyLightRotateTime;

	public Color sunMiddayColour;
	public Color sunDuskDawnColour;
	
	public Color moonMidnightColour;
	public Color moonDuskDawnColour;

	private bool isDaytime;
	private bool tripStarted;
	private bool chillTrip;
	
	private float skyRotateTime;
	private float lightRotateTime;


	private Light currentSun;			// alternates in paranoia
	private Light currentMoon;			// alternates in paranoia


	private void Start()
	{
		skyRotateTime = startSkyRotateTime;
		lightRotateTime = startLightRotateTime;
		
		sun.color = sunDuskDawnColour;
		moon.color = moonDuskDawnColour;

		currentSun = sun;
		currentMoon = moon;
		
		SetDaytime(true);
		
		// rotate sun and moon around x axis	
		RotateSun();
		RotateMoon();
		
		RotateSunColours();
		RotateMoonColours();
	}
	
	private void OnEnable()
    {
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnSwitchPaintLayer += OnSwitchPaintLayer;
	}
	
	private void OnDisable()
    {
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnSwitchPaintLayer -= OnSwitchPaintLayer;
	}


	private void Update()
    {
		// rotate sky slowly around x axis
		if (tripStarted)
        	transform.RotateAround(transform.position, Vector3.right, skyRotateTime * Time.deltaTime);
    }
    
    private void OnTripStart(bool isChill)
	{
		tripStarted = true;
		chillTrip = isChill;
		
		skyRotateTime = isChill ? chillSkyRotateTime : anxietySkyRotateTime;
		lightRotateTime = isChill ? chillLightRotateTime : anxietyLightRotateTime;
	}
    
    
	private void SetDaytime(bool daytime)
	{
		if (daytime)
		{
			TripEvents.OnSunrise?.Invoke();
			isDaytime = true;
		}
		else
		{
			TripEvents.OnSunset?.Invoke();
			isDaytime = false;
		}
	}
	

	private void RotateSun()
	{
		LeanTween.rotateAround(sun.gameObject, Vector3.right, 180f, lightRotateTime)
				.setEase(LeanTweenType.linear)
				.setOnComplete(() =>
				{
					SetDaytime(!isDaytime);
					RotateSun();
				});
	}

		       	
	private void RotateMoon()
	{
		LeanTween.rotateAround(moon.gameObject, Vector3.right, 180f, lightRotateTime)
				 .setEase(LeanTweenType.linear)
                .setOnComplete(() =>
                {
					RotateMoon();
                });
	}

	private void RotateSunColours()
	{
		LeanTween.value(gameObject, sunDuskDawnColour, sunMiddayColour, lightRotateTime / 2f)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					currentSun.color = val;
				})
				.setLoopPingPong();
	}
	
	private void RotateMoonColours()
	{
		LeanTween.value(gameObject, moonDuskDawnColour, moonMidnightColour, lightRotateTime / 2f)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					currentMoon.color = val;
				})
                .setLoopPingPong();
	}
	
	
	private void OnSwitchPaintLayer()
	{
		if (chillTrip)
			return;
			
		// switch sun and moon... so colours can be 'swapped' until next event
		var savedSun = currentSun;

		currentSun = currentMoon;
		currentMoon = savedSun;
	}
}
