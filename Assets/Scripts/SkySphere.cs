﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkySphere : MonoBehaviour
{
	public List<Transform> objectLayers = new List<Transform>();

    public Transform skyObject;         // sphere collider
    
	public ParticleSystem stars;
    
	private int paintLayerIndex = 0;
	private bool chillTrip;

	public Transform CurrentPaintLayer { get { return objectLayers[paintLayerIndex]; } }

	private void OnEnable()
	{    
        TripEvents.OnTripRestart += OnRestart;    
        TripEvents.OnTripStart += OnTripStart;    
        TripEvents.OnSwitchPaintLayer += OnSwitchPaintLayer;    
        TripEvents.OnSunrise += OnSunrise;
        TripEvents.OnSunset += OnSunset;
	}

	private void OnDisable()
	{
        TripEvents.OnTripRestart -= OnRestart; 
        TripEvents.OnTripStart -= OnTripStart;     
        TripEvents.OnSwitchPaintLayer -= OnSwitchPaintLayer;   
        TripEvents.OnSunrise -= OnSunrise;
        TripEvents.OnSunset -= OnSunset;       
    }

	public void ClearSpawned()
	{
		foreach (var layer in objectLayers)
		{
			foreach (Transform child in layer.transform)
			{
				Destroy(child.gameObject);
			}

			// sphere should have no children!
			foreach (Transform child in skyObject.transform)
			{
				Destroy(child.gameObject);
			}
		}
    }
  
    
    private void OnSwitchPaintLayer()
	{
		if (chillTrip)
			return;
			
		if (objectLayers.Count <= 1)
			return;

		CurrentPaintLayer.gameObject.SetActive(false);

		if (paintLayerIndex == objectLayers.Count - 1)
			paintLayerIndex = 0;
		else
			paintLayerIndex++;
			
		CurrentPaintLayer.gameObject.SetActive(true);
	}
    
        
    private void OnTripStart(bool isChill)
    {
        chillTrip = isChill;
        stars.Stop();
    }
    
	private void OnRestart()
	{
		ClearSpawned();
		stars.Stop();
	}
	
			
	private void OnSunset()
	{
		stars.Play();
	}

	private void OnSunrise()
	{
		stars.Stop();
	}
}