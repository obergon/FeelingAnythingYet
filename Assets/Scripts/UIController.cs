﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	public Text beatCountText;
	public Text musicTime;
	public Text musicMood;
	public Text tripType;
	
	public Image creditsPanel;

	private DateTime musicStartTime;
	private bool musicPlaying = false;

	private Coroutine musicTimerCoroutine;
	
	
	private void Start()
    {
		musicPlaying = false;
        creditsPanel.enabled = false;
    }
	
    private void OnEnable()
    {
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnTripRestart += OnTripRestart;
        TripEvents.OnMusicStarted += OnMusicStarted;
        TripEvents.OnBeatStarted += OnBeatStarted;
        TripEvents.OnMusicBeat += OnMusicBeat;
        TripEvents.OnMusicMoodChange += OnMusicMoodChange;
        //TripEvents.OnMusicOutro += OnMusicOutro;
        TripEvents.OnRollCredits += OnRollCredits;
	}

	private void OnDisable()
    {
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnTripRestart -= OnTripRestart;
        TripEvents.OnMusicStarted -= OnMusicStarted;
        TripEvents.OnBeatStarted -= OnBeatStarted;
        TripEvents.OnMusicBeat -= OnMusicBeat;
        TripEvents.OnMusicMoodChange -= OnMusicMoodChange;
        //TripEvents.OnMusicOutro -= OnMusicOutro;
        TripEvents.OnRollCredits -= OnRollCredits;
	}

	private void OnBeatStarted(int beatCount, float barInterval, int beatsPerBar)
	{
		beatCountText.text = beatCount + "/" + beatsPerBar;
	}

	private void OnMusicBeat(float barInterval, int beatsPerBar, int beatCount, float curveValue)
	{
		beatCountText.text = beatCount + "/" + beatsPerBar;
	}
	
	private void OnTripStart(bool isChill)
	{
		tripType.text = isChill ? "Chill" : "Paranoid";
	}
	
	//private void OnMusicOutro()
	//{
	//    creditsPanel.gameObject.SetActive(true);
	//	musicMood.text = "Outro";
	//}
	
	private void OnRollCredits()
	{
	    creditsPanel.gameObject.SetActive(true);
	}
	
	private void OnTripRestart()
	{
		musicPlaying = false;
		StopMusicTimer();
		creditsPanel.gameObject.SetActive(false);
	}

	private void OnMusicStarted(DateTime startTime, float barInterval, int beatsPerBar)
	{
		musicStartTime = startTime;
		musicPlaying = true;

		StopMusicTimer();
		musicTimerCoroutine = StartCoroutine(MusicTimer());
		
	    creditsPanel.gameObject.SetActive(false);
	}
	
	private void OnMusicMoodChange(BeatConductor.MusicMood mood)
	{
		musicMood.text = mood.ToString();
	}

	private void StopMusicTimer()
	{
		if (musicTimerCoroutine != null)
		{
			StopCoroutine(musicTimerCoroutine);
			musicTimerCoroutine = null;
		}
	}

	private IEnumerator MusicTimer()
	{
		int musicSecs = 0;
		
		while (musicPlaying)
		{
			int secs = musicSecs % 60;
			int mins = musicSecs / 60;
			musicTime.text = mins + ":" + (secs < 10 ? "0" : "") + secs;
			
			yield return new WaitForSecondsRealtime(1f);
			musicSecs++;
		}

		yield return null;
	}
}
