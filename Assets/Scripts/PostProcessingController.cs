﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessingController : MonoBehaviour
{
    private bool isChill = true;
    public PostProcessVolume volume;

    private Bloom _Bloom;
    private LensDistortion _LensDistortion;
    private Vignette _Vignette;
    private Grain _Grain;
    private ChromaticAberration _ChromaticAberration;

    [Header("IntensityValues")]
    public float bloomIntensity;
    public float lensDistortionIntensity;
    public float vignetteIntensity;
    public float grainIntensity;
    public float chromaticAberrationIntensity;


    private void OnEnable()
    {
        //TripEvents.OnMusicBeat += OnBeat;
        //TripEvents.OnMusicMoodChange += OnMusicMoodChange;
        TripEvents.OnTripStart += OnTripStart;
    }

    private void OnDisable()
    {
        //TripEvents.OnMusicBeat -= OnBeat;
        //TripEvents.OnMusicMoodChange -= OnMusicMoodChange;
        TripEvents.OnTripStart -= OnTripStart;
    }
    
    private void Start()
    {
        volume.profile.TryGetSettings(out _Bloom);
        volume.profile.TryGetSettings(out _LensDistortion);
        volume.profile.TryGetSettings(out _Vignette);
        volume.profile.TryGetSettings(out _Grain);
        volume.profile.TryGetSettings(out _ChromaticAberration);
    }
    
    //private void Update()
    //{
    //    NormalModeUpdate();
    //    IntenseModeUpdate();
    //    QuietModeUpdate();
    //}


    private void OnTripStart(bool chill)
    {
        isChill = chill;
        
        if (isChill) // Chill mode
        {
            bloomIntensity = 2f;
            lensDistortionIntensity = 0;
            vignetteIntensity = 0;
            grainIntensity = 0;
            chromaticAberrationIntensity = 0;
        }
        else // Anxious mode
        {
            bloomIntensity = 3f;
            lensDistortionIntensity = -70f;
            vignetteIntensity = 0.4f;
            grainIntensity = 0.5f;
            chromaticAberrationIntensity = 0.5f;
        }

		SetPostProcessing();
    }


	private void SetPostProcessing()
	{
	    volume.profile.TryGetSettings(out _Bloom);
        volume.profile.TryGetSettings(out _LensDistortion);
        volume.profile.TryGetSettings(out _Vignette);
        volume.profile.TryGetSettings(out _Grain);
        volume.profile.TryGetSettings(out _ChromaticAberration);
        
		_Bloom.intensity.value = bloomIntensity;
		_LensDistortion.intensity.value = lensDistortionIntensity;
		_Vignette.intensity.value = vignetteIntensity;
		_Grain.intensity.value = grainIntensity;
		_ChromaticAberration.intensity.value = chromaticAberrationIntensity;
	}
	
	
    //private void OnMusicMoodChange(BeatConductor.MusicMood mood)
    //{
        //Debug.Log(mood);
        
        //switch (mood)
        //{
        //    case BeatConductor.MusicMood.Normal:
        //        Debug.Log("NormalMode");
        //        NormalMode();
        //        break;
        //    case BeatConductor.MusicMood.Intense:
        //        Debug.Log("IntenseMode");
        //        IntenseMode();
        //        break;
        //    case BeatConductor.MusicMood.Quiet:
        //        Debug.Log("QuietMode");
        //        QuietMode();
        //        break;
        //}
    //}


    //private void NormalModeUpdate()
    //{
    //    if (isChill) // Chill mode
    //    {
    //        bloomIntensity = 1;
    //        lensDistortionIntensity = 1;
    //        vignetteIntensity = 1;
    //        grainIntensity = 1;
    //        chromaticAberrationIntensity = 1;
    //    }
    //    else // Anxious mode
    //    {
    //        bloomIntensity = 1;
    //        lensDistortionIntensity = 1;
    //        vignetteIntensity = 1;
    //        grainIntensity = 1;
    //        chromaticAberrationIntensity = 1;
    //    }
    //}
    
    //private void IntenseModeUpdate()
    //{
    //    if (isChill) // Chill mode
    //    {
    //        bloomIntensity = 1;
    //        lensDistortionIntensity = 1;
    //        vignetteIntensity = 1;
    //        grainIntensity = 1;
    //        chromaticAberrationIntensity = 1;
    //    }
    //    else // Anxious mode
    //    {
    //        bloomIntensity = 1;
    //        lensDistortionIntensity = 1;
    //        vignetteIntensity = 1;
    //        grainIntensity = 1;
    //        chromaticAberrationIntensity = 1;
    //    }
    //}
    
    //private void QuietModeUpdate()
    //{
    //    if (isChill) // Chill mode
    //    {
    //        bloomIntensity = 1;
    //        lensDistortionIntensity = 1;
    //        vignetteIntensity = 1;
    //        grainIntensity = 1;
    //        chromaticAberrationIntensity = 1;
    //    }
    //    else // Anxious mode
    //    {
    //        bloomIntensity = 1;
    //        lensDistortionIntensity = 1;
    //        vignetteIntensity = 1;
    //        grainIntensity = 1;
    //        chromaticAberrationIntensity = 1;
    //    }
    //}
    
    //private void NormalMode()
    //{
        //volume.profile.TryGetSettings(out _Bloom);
        //volume.profile.TryGetSettings(out _LensDistortion);
        //volume.profile.TryGetSettings(out _Vignette);
        //volume.profile.TryGetSettings(out _Grain);
        //volume.profile.TryGetSettings(out _ChromaticAberration);

        //if (isChill) // Chill mode
        //{
            //_Bloom.intensity.value = bloomIntensity;
            //_LensDistortion.intensity.value = lensDistortionIntensity;
            //_Vignette.intensity.value = vignetteIntensity;
            //_Grain.intensity.value = grainIntensity;
            //_ChromaticAberration.intensity.value = chromaticAberrationIntensity;
        //}
        //else // Anxious mode
        //{

            //_Bloom.intensity.value = bloomIntensity;
            //_LensDistortion.intensity.value = lensDistortionIntensity;
            //_Vignette.intensity.value = vignetteIntensity;
            //_Grain.intensity.value = grainIntensity;
            //_ChromaticAberration.intensity.value = chromaticAberrationIntensity;
        //}
    //}
    
    //private void IntenseMode()
    //{
    //    volume.profile.TryGetSettings(out _Bloom);
    //    volume.profile.TryGetSettings(out _LensDistortion);
    //    volume.profile.TryGetSettings(out _Vignette);
    //    volume.profile.TryGetSettings(out _Grain);
    //    volume.profile.TryGetSettings(out _ChromaticAberration);

    //    if (isChill) // Chill mode
    //    {

    //        _Bloom.intensity.value = bloomIntensity;
    //        _LensDistortion.intensity.value = lensDistortionIntensity;
    //        _Vignette.intensity.value = vignetteIntensity;
    //        _Grain.intensity.value = grainIntensity;
    //        _ChromaticAberration.intensity.value = chromaticAberrationIntensity;
    //    }
    //    else // Anxious mode
    //    {

    //        _Bloom.intensity.value = bloomIntensity;
    //        _LensDistortion.intensity.value = lensDistortionIntensity;
    //        _Vignette.intensity.value = vignetteIntensity;
    //        _Grain.intensity.value = grainIntensity;
    //        _ChromaticAberration.intensity.value = chromaticAberrationIntensity;
    //    }
    //}
    
    //private void QuietMode()
    //{
    //    volume.profile.TryGetSettings(out _Bloom);
    //    volume.profile.TryGetSettings(out _LensDistortion);
    //    volume.profile.TryGetSettings(out _Vignette);
    //    volume.profile.TryGetSettings(out _Grain);
    //    volume.profile.TryGetSettings(out _ChromaticAberration);

    //    if (isChill) 
    //    {
    //        _Bloom.intensity.value = bloomIntensity;
    //        _LensDistortion.intensity.value = lensDistortionIntensity;
    //        _Vignette.intensity.value = vignetteIntensity;
    //        _Grain.intensity.value = grainIntensity;
    //        _ChromaticAberration.intensity.value = chromaticAberrationIntensity;

    //    }
    //    else // Anxious mode
    //    {

    //        _Bloom.intensity.value = bloomIntensity;
    //        _LensDistortion.intensity.value = lensDistortionIntensity;
    //        _Vignette.intensity.value = vignetteIntensity;
    //        _Grain.intensity.value = grainIntensity;
    //        _ChromaticAberration.intensity.value = chromaticAberrationIntensity;
    //    }
    //}
    
    private void OnBeat(float barInterval, int beatsPerBar, int beatCount, float curveValue)
    {

    }
}
