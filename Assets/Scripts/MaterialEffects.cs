﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MaterialController))]
public class MaterialEffects : MonoBehaviour
{
    class MaterialPropertyGroup
    {
        MeshRenderer meshRenderer;
        MaterialPropertyBlock[] propertyBlocks;

        public MaterialPropertyGroup(MeshRenderer _meshRenderer)
        {
            meshRenderer = _meshRenderer;

            // create the property blocks
            propertyBlocks = new MaterialPropertyBlock[meshRenderer.sharedMaterials.Length];
            for (int index = 0; index < propertyBlocks.Length; ++index)
            {
                propertyBlocks[index] = new MaterialPropertyBlock();

                // enable emission
                meshRenderer.sharedMaterials[index].EnableKeyword("_EMISSION");
            }
        }

        public void UpdateColour(Color colour)
        {
            // update the transparency
            for (int index = 0; index < propertyBlocks.Length; ++index)
            {
                // read the property block
                meshRenderer.GetPropertyBlock(propertyBlocks[index], index);

                // read the current colour
                Color currentColour = propertyBlocks[index].GetColor("_Color");

                // update the color
                currentColour = colour;

                // write the new colour
                propertyBlocks[index].SetColor("_Color", currentColour);

                // update the property block
                meshRenderer.SetPropertyBlock(propertyBlocks[index], index);
            }
        }

        public void UpdateTransparency(float newTransparency)
        {
            // update the transparency
            for (int index = 0; index < propertyBlocks.Length; ++index)
            {
                // read the property block
                meshRenderer.GetPropertyBlock(propertyBlocks[index], index);

                // read the current colour
                Color currentColour = propertyBlocks[index].GetColor("_Color");

                // update the transparency
                currentColour.a = newTransparency;

                // write the new colour
                propertyBlocks[index].SetColor("_Color", currentColour);

                // update the property block
                meshRenderer.SetPropertyBlock(propertyBlocks[index], index);
            }
        }

        public void UpdateEmission(Color emissionColour)
        {
            // update the transparency
            for (int index = 0; index < propertyBlocks.Length; ++index)
            {
                // read the property block
                meshRenderer.GetPropertyBlock(propertyBlocks[index], index);

                // write the emission colour
                propertyBlocks[index].SetColor("_EmissionColor", emissionColour);

                // update the property block
                meshRenderer.SetPropertyBlock(propertyBlocks[index], index);
            }
        }
    }

    List<MaterialPropertyGroup> MaterialPropertyGroups = new List<MaterialPropertyGroup>();

    // inspector properties
    [Range(0f, 1f)]
    public float Transparency = 1f;

    public Color EmissionColour;
    public Color colour;


    void Awake()
    {
        // retrieve all of the mesh renderers
        MeshRenderer[] allMeshRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in allMeshRenderers)
        {
            MaterialPropertyGroups.Add(new MaterialPropertyGroup(mr));
        }
    }

    // for testing via inspector values
    void Update()
    {
        //UpdateColour(colour);
        //UpdateTransparency(Transparency);
        //UpdateEmission(EmissionColour);
    }

    public void UpdateColour(Color color)
    {
        foreach (MaterialPropertyGroup propertyGroup in MaterialPropertyGroups)
        {
            propertyGroup.UpdateColour(color);
        }
    }

    public void UpdateTransparency(float trans)
    {
        foreach (MaterialPropertyGroup propertyGroup in MaterialPropertyGroups)
        {
            propertyGroup.UpdateTransparency(trans);
        }
    }

    public void UpdateEmission(Color emission)
    {
        foreach (MaterialPropertyGroup propertyGroup in MaterialPropertyGroups)
        {
            propertyGroup.UpdateEmission(emission);
        }
    }
}
