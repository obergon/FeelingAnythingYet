﻿using System;
using UnityEngine;

public static class TripEvents
{
	public delegate void OnMusicStartedDelegate(DateTime musicStartTime, float barInterval, int beatsPerBar);
	public static OnMusicStartedDelegate OnMusicStarted;

    public delegate void OnMusicMoodChangeDelegate(BeatConductor.MusicMood mood);
    public static OnMusicMoodChangeDelegate OnMusicMoodChange;
    
    public delegate void OnMusicGuitarDelegate();
    public static OnMusicGuitarDelegate OnMusicGuitar;
    
    public delegate void OnMusicDrumsDelegate();
    public static OnMusicDrumsDelegate OnMusicDrums;
        
    public delegate void OnMusicBassDelegate();
    public static OnMusicBassDelegate OnMusicBass;

    public delegate void OnBeatStartedDelegate(int beatCount, float barInterval, int beatsPerBar);
	public static OnBeatStartedDelegate OnBeatStarted;
	
	public delegate void OnMusicBeatDelegate(float barInterval, int beatsPerBar, int beatCount, float curveValue);
	public static OnMusicBeatDelegate OnMusicBeat;

    public delegate void OnMusicHalfBeatDelegate(float barInterval, int beatsPerBar, int halfBeatCount, float curveValue);
    public static OnMusicHalfBeatDelegate OnMusicHalfBeat;

    public delegate void OnMusicQtrBeatDelegate(float barInterval, int beatsPerBar, int qtrBeatCount, float curveValue);
	public static OnMusicHalfBeatDelegate OnMusicQtrBeat;

	//public delegate void OnMusicBarDelegate(float barInterval, int beatsPerBar, float curveValue);
	//public static OnMusicBarDelegate OnMusicBar;

    public delegate void OnObjectFadeOutDelegate(MaterialController fadingObject);
    public static OnObjectFadeOutDelegate OnObjectFadeOut;
    
    public delegate void OnPlayerInputDelegate();
	public static OnPlayerInputDelegate OnPlayerInput;
	
	public delegate void OnSwitchPaintLayerDelegate();
	public static OnSwitchPaintLayerDelegate OnSwitchPaintLayer;
	
	public delegate void OnTripStartDelegate(bool isChill);
	public static OnTripStartDelegate OnTripStart;
	
	public delegate void OnPlayerIdleDelegate(bool isIdle, bool forceCut);
	public static OnPlayerIdleDelegate OnPlayerIdle;
	
	public delegate void OnMusicOutroDelegate();
	public static OnMusicOutroDelegate OnMusicOutro;
	
	public delegate void OnRollCreditsDelegate();
	public static OnRollCreditsDelegate OnRollCredits;
	
	public delegate void OnTripEndDelegate();
	public static OnTripEndDelegate OnTripEnd;
		
	public delegate void OnRestartDelegate();
	public static OnRestartDelegate OnTripRestart;
	
	public delegate void OnSunsetDelegate();
	public static OnSunsetDelegate OnSunset;
		
	public delegate void OnSunriseDelegate();
	public static OnSunriseDelegate OnSunrise;
	
	public delegate void OnSpotLightsDelegate(bool lightsOn);
    public static OnSpotLightsDelegate OnSpotLights;
}

