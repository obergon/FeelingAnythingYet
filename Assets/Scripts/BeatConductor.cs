﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BeatConductor : MonoBehaviour
{
	public AudioSource cricketsAudio; 
	public AudioSource windAudio; 
	public AudioSource gameMusic;

	public float crossfadeTime;		// crickets/wind -> music
	//public float introPause;
	public float barInterval;
	public int beatsPerBar = 4;

    public AudioClip tickSound;
    public Animator timeline;
    public AnimationCurve animationCurve;

	private float cricketsVolume;
	private float windVolume;
	private float musicVolume;

    private bool clockTicking = false;
	private Coroutine clockCoroutine;

    private DateTime musicStartTime;
	private float SecondsIntoMusic { get { return (DateTime.Now - musicStartTime).Seconds; }}
	

    public enum MusicMood
    {
        Normal,
        Intense,
        Quiet
    }

	private void Start()
	{
		//timeline.enabled = false;
		
		musicVolume = gameMusic.volume;
		cricketsVolume = cricketsAudio.volume;
		windVolume = windAudio.volume;
		FadeInIntroAudio();
		
		NormalMood();
	}

	
	private void OnEnable()
    {
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnTripEnd += OnTripEnd;
        TripEvents.OnTripRestart += OnTripRestart;
        TripEvents.OnMusicBass += OnMusicBass;
    }

    private void OnDisable()
    {
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnTripEnd -= OnTripEnd;
        TripEvents.OnTripRestart -= OnTripRestart;
        TripEvents.OnMusicBass -= OnMusicBass;
	}


	private void OnTripStart(bool isChill)
	{
		timeline.SetTrigger("StartMusic");
		FadeInGameMusic();
	}
	

	private void OnTripEnd()
	{
		StopMusic();
		FadeInIntroAudio();
		NormalMood();
	}
	
	private void OnTripRestart()
	{
		timeline.SetTrigger("StopMusic");	
	}
	    
    [ContextMenu("FadeInIntroAudio")]
    public void FadeInIntroAudio()
    {
		cricketsAudio.volume = 0f;
		windAudio.volume = 0f;
		cricketsAudio.Play();
		windAudio.Play();
				
		LeanTween.value(0, cricketsVolume, crossfadeTime)
					 .setOnUpdate(x => cricketsAudio.volume = x);
					 
		LeanTween.value(0, windVolume, crossfadeTime)
					 .setOnUpdate(x => windAudio.volume = x);

		LeanTween.value(musicVolume, 0f, crossfadeTime)
				 .setOnUpdate(x => gameMusic.volume = x);
    }


	[ContextMenu("FadeInGameMusic")]
    public void FadeInGameMusic()
    {
		gameMusic.volume = 0f;
    	StartMusic();

		LeanTween.value(0f, musicVolume, crossfadeTime)
				 .setOnUpdate(x => gameMusic.volume = x);
    		
    	// fade out intro audio
		LeanTween.value(cricketsAudio.volume, 0, crossfadeTime)
					 .setOnUpdate(x => cricketsAudio.volume = x)
					 .setOnComplete(() =>
					 {
						cricketsAudio.Stop();
					 });
					 
		LeanTween.value(windAudio.volume, 0, crossfadeTime)
					 .setOnUpdate(x => windAudio.volume = x)
					 .setOnComplete(() =>
					 {
						windAudio.Stop();
					 });
    }

	
	private void StartMusic()
	{
		StopMusic();
		
		gameMusic.Play();
        musicStartTime = DateTime.Now;
        TripEvents.OnMusicStarted?.Invoke(musicStartTime, barInterval, beatsPerBar);

		//clockTicking = true;
		//clockCoroutine = StartCoroutine(AudioClock());
	}
	
	private void OnMusicBass()
	{
		clockTicking = true;
		clockCoroutine = StartCoroutine(AudioClock());
	}
	
	private IEnumerator AudioClock()
	{
		//yield return new WaitForSeconds(introPause);
		Debug.Log("AudioClock");

		//bool firstBeat = true;
		int beatCount = 1;
		int halfBeatCount = 0;
        int qtrBeatCount = 0;

		TripEvents.OnBeatStarted?.Invoke(beatCount, barInterval, beatsPerBar);

        while (clockTicking)
		{
            //AudioSource.PlayClipAtPoint(tickSound, Vector3.zero, 0.5f);
            qtrBeatCount++;
            yield return new WaitForSecondsRealtime(barInterval / (beatsPerBar * 4f));
            TripEvents.OnMusicQtrBeat?.Invoke(barInterval, beatsPerBar, qtrBeatCount, animationCurve.Evaluate(SecondsIntoMusic));

            if (qtrBeatCount > 0 && qtrBeatCount % 2 == 0)        // on half beat
            {
                halfBeatCount++;
                TripEvents.OnMusicHalfBeat?.Invoke(barInterval, beatsPerBar, halfBeatCount, animationCurve.Evaluate(SecondsIntoMusic));
            }

			if (qtrBeatCount > 0 && qtrBeatCount % 4 == 0)      // on beat
            {
                beatCount++;
                TripEvents.OnMusicBeat?.Invoke(barInterval, beatsPerBar, beatCount, animationCurve.Evaluate(SecondsIntoMusic));
			}

			if (beatCount >= beatsPerBar)
			{		
				TripEvents.OnSwitchPaintLayer?.Invoke();	// switch paint layers on each bar
				
				//TripEvents.OnMusicBar?.Invoke(barInterval, beatsPerBar, animationCurve.Evaluate(SecondsIntoMusic));
				beatCount = 0;
				halfBeatCount = 0;
			}

			//if (firstBeat)
			//{
			//	TripEvents.OnBeatStarted?.Invoke(barInterval, beatsPerBar);
			//	firstBeat = false;
			//}
		}
	}
	
	private void StopMusic()
	{
		gameMusic.Stop();
		clockTicking = false;

		if (clockCoroutine != null)
		{
			StopCoroutine(clockCoroutine);
			clockCoroutine = null;
		}
	}
    
  //  [ContextMenu("StopAll")]
  //  public void StopAll()
  //  {
		//foreach (var intro in introAudio)
		//{
		//	intro.Stop();
		//}

		//StopMusic();
    //}

    // animation events
    public void Guitar()
    {
		Debug.Log("Guitar");
        TripEvents.OnMusicGuitar?.Invoke();
    }
    
    public void Drums()
    {
		Debug.Log("Drums");
        TripEvents.OnMusicDrums?.Invoke();
    }
    
   
    public void Bass()
    {
		Debug.Log("Bass");
        TripEvents.OnMusicBass?.Invoke();
    }
  
    
    public void NormalMood()
    {
        TripEvents.OnMusicMoodChange?.Invoke(MusicMood.Normal);
    }

    public void QuietMood()
    {
        TripEvents.OnMusicMoodChange?.Invoke(MusicMood.Quiet);
    }

    public void ManicMood()
    {
        TripEvents.OnMusicMoodChange?.Invoke(MusicMood.Intense);
    }

    public void Outro()
    {
        TripEvents.OnMusicOutro?.Invoke();
    }
    
    public void Credits()
    {
        TripEvents.OnRollCredits?.Invoke();
    }
    
    public void EndTrip()
    {
		//OnTripEnd();
        Debug.Log("EndTrip animation event");
        TripEvents.OnTripEnd?.Invoke();
    }
    
    public void Restart()
    {
        Debug.Log("Restart animation event");
        TripEvents.OnTripRestart?.Invoke();
    }
}