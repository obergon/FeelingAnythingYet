﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour
{
	public Image blackOut;
	public float blackOutTime;
	public Color blackOutColour = Color.black;

	public float startBlackOutTime = 3f;
	public float restartBlackOutTime = 6f;
	
	private bool blackedOut = false;
	
	
	private void OnEnable()
	{
		TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnTripRestart += OnRestart;
	}

	private void OnDisable()
	{
		TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnTripRestart -= OnRestart;
	}


	private void BlackOut(float blackedOutTime)
	{
		if (blackedOut)
			return;
			
		blackOut.color = Color.clear;
		blackOut.gameObject.SetActive(true);

		LeanTween.value(gameObject, Color.clear, blackOutColour, blackOutTime)
						.setEase(LeanTweenType.easeOutCubic)
						.setOnUpdate(x => blackOut.color = x)
						.setOnComplete(() =>
						{
							blackedOut = true;
							ClearBlackout(blackedOutTime);	// tween to clear
						});
	}
	
	private void ClearBlackout(float blackedOutTime)
	{
		if (!blackedOut)
			return;
			
		// fade back to clear
		LeanTween.value(gameObject, blackOutColour, Color.clear, blackOutTime)
						.setEase(LeanTweenType.easeInCubic)
						.setDelay(blackedOutTime)
						.setOnUpdate(x => blackOut.color = x)
						.setOnComplete(() =>
						{
							blackOut.gameObject.SetActive(false);
							blackedOut = false;
						});
	}
	
	
	private void OnTripStart(bool isChill)
	{
		BlackOut(startBlackOutTime);
	}
	
	private void OnRestart()
	{
		BlackOut(restartBlackOutTime);
	}
}
