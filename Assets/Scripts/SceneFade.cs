﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SceneFade : MonoBehaviour
{
    public Image fadeOutImage;
    public float fadeTime;

    private void Start()
    {
        fadeOutImage.canvasRenderer.SetAlpha(0);
    }

    public void fadeOut()
    {
        fadeOutImage.CrossFadeAlpha(1, fadeTime, false);
    }
    
    public void fadeIn()
    {
        fadeOutImage.CrossFadeAlpha(0, fadeTime, false);
    }
}
