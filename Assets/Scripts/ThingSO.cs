﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Thing Prefab")]
public class ThingSO : ScriptableObject
{
	//public string description;
    public GameObject prefab;
	[Space]
    public bool onSky;
    public bool onGround;
	[Space]
	public bool onPath;
	public bool outsidePath;
	[Space]
	public bool inDaytime;
	public bool atNight;
	[Space]
	public bool isChill;
	public bool isAnxiety;
}
