﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Prefab Group")]
public class ObjectGroupSO : ScriptableObject
{
    public ThingSO[] thingPrefabs;
    
 //   private List<ThingSO> MatchingObjects(bool isDay, bool onSky, bool onPath, bool chill)
	//{
	//	if (onSky)
	//		return thingPrefabs.Where(x => x.inDaytime == isDay && x.onSky &&
	//									x.isChill == chill).ToList();
	//	else
	//		return thingPrefabs.Where(x => x.inDaytime == isDay && !x.onSky &&
	//									x.onPath == onPath && x.isChill == chill).ToList();
	//}
	
	private List<ThingSO> MatchingObjects(bool isDay, bool onSky, bool onPath, bool chill)
	{
		List<ThingSO> filteredList = new List<ThingSO>();

		foreach (var thing in thingPrefabs)
		{
			if (isDay && !thing.inDaytime)
				continue;
			if (!isDay && !thing.atNight)
				continue;
				
			if (onSky && !thing.onSky)
				continue;
			if (!onSky && !thing.onGround)
				continue;

			if (!onSky)
			{
				if (onPath && !thing.onPath)
					continue;
				if (!onPath && !thing.outsidePath)
					continue;
			}
				
			if (chill && !thing.isChill)
				continue;
			if (!chill && !thing.isAnxiety)
				continue;

			filteredList.Add(thing);
		}

		return filteredList;
	}

	public GameObject RandomThingPrefab(bool isDay, bool onSky, bool onPath, bool chill)
	{
		var thingList = MatchingObjects(isDay, onSky, onPath, chill);
		if (thingList == null || thingList.Count == 0)
			return null;
			
		return thingList [ Random.Range(0, thingList.Count) ].prefab;
	}
}
