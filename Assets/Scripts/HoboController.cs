﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HoboController : MonoBehaviour
{
    public float moveSpeed;
    public Transform spellCastPoint;

    public float turnAngle;             // +/- - increment
    public float maxTurnAngle;          // +/-
    public float rollTime;             	// for rotation lerp

    public float xLimit;                    // left/right movement constraint

    private Vector3 rollRotation = Vector3.zero;
    private float rollZAngle;

    private float xInput;                    // A/D or arrow keys
    private bool inOutro = false;

    private bool idleMode = false;
    private Coroutine autoMoveCoroutine;
    private float autoMoveInterval = 0.05f; // 0.0005f;

    private float autoMoveIncrement = 0.075f;
    private float autoMoveDistance = 0f;

    private float outroDistance = 150f;
    //private float outroTime = 60f;			// until restart event
    private float outroSpeed = 0.1f;

    private bool guitarStarted = false;

    private Vector3 startPosition;


    private void Awake()
    {
        startPosition = transform.position;
        LeanTween.init(32000);
    }

    private void OnEnable()
    {
        TripEvents.OnPlayerIdle += OnPlayerIdle;
        //TripEvents.OnBeatStarted += OnBeatStarted;
        TripEvents.OnMusicGuitar += OnGuitar;
        TripEvents.OnMusicOutro += OnMusicOutro;
        TripEvents.OnTripRestart += OnTripRestart;
    }

    private void OnDisable()
    {
        TripEvents.OnPlayerIdle -= OnPlayerIdle;
        //TripEvents.OnBeatStarted -= OnBeatStarted;
        TripEvents.OnMusicGuitar -= OnGuitar;
        TripEvents.OnMusicOutro -= OnMusicOutro;
        TripEvents.OnTripRestart -= OnTripRestart;
    }


    private void Update()
    {
        if (inOutro)
            return;

        xInput = Input.GetAxis("Horizontal");

        if (Mathf.Abs(xInput) > 0f)
        {
            TripEvents.OnPlayerInput?.Invoke();

            SidewaysMove(xInput);
            autoMoveDistance = xInput < 0 ? -1f : 1f;          // maintain direction of player input
        }
    }

    private bool SidewaysMove(float increment)
    {
        //Debug.Log("Move: " + increment);
        bool limited = false;

        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x + increment,
                        transform.position.y, transform.position.z), moveSpeed * Time.deltaTime);

        // head to the horizon in outro (ground stops, camera slows)
        if (inOutro)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x,
                        transform.position.y, transform.position.z + outroDistance), outroSpeed * Time.deltaTime);
        }

        // clamp to left/right limits
        if (transform.position.x > xLimit)
        {
            limited = true;
            transform.position = Vector3.Lerp(transform.position, new Vector3(xLimit,
                                        transform.position.y, transform.position.z), moveSpeed * Time.deltaTime);
        }
        else if (transform.position.x < -xLimit)
        {
            limited = true;
            transform.position = Vector3.Lerp(transform.position, new Vector3(-xLimit,
                                        transform.position.y, transform.position.z), moveSpeed * Time.deltaTime);
        }

        // roll to the side...
        rollRotation = new Vector3(rollRotation.x, rollRotation.y, Mathf.LerpAngle(rollRotation.z,
                                                    RollZAngle(increment, true), Time.deltaTime * rollTime));
        transform.localEulerAngles = rollRotation;

        return limited;
    }


    private IEnumerator AutoSwing()
    {
        //Debug.Log("AutoSwing");

        while (idleMode || inOutro)
        {
            if (Mathf.Abs(autoMoveDistance) < 1f)
            {
                autoMoveDistance += autoMoveDistance >= 0f ? autoMoveIncrement : -autoMoveIncrement;
                //autoMoveDistance = Mathf.Clamp(autoMoveDistance, -1f, 1f);
            }

            if (SidewaysMove(autoMoveDistance))            // true if reached left/right limits
                autoMoveDistance = (autoMoveDistance > 0f) ? -autoMoveIncrement : autoMoveIncrement;

            yield return new WaitForSeconds(autoMoveInterval);
        }

        yield return null;
    }


    private void AutoSwingOn()
    {
        AutoSwingOff();
        autoMoveCoroutine = StartCoroutine(AutoSwing());
    }

    private void AutoSwingOff()
    {
        if (autoMoveCoroutine != null)
        {
            StopCoroutine(autoMoveCoroutine);
            autoMoveCoroutine = null;
        }
    }

    private void OnPlayerIdle(bool isIdle, bool forceCut)
    {
        if (idleMode == isIdle)
            return;

        idleMode = isIdle;

        if (!guitarStarted)
            return;

        if (isIdle)
            AutoSwingOn();
        else
            AutoSwingOff();
    }


    private void OnTripRestart()
    {
        inOutro = false;
        guitarStarted = false;
        inOutro = false;
        idleMode = false;

        AutoSwingOff();

        transform.position = startPosition;
        transform.rotation = Quaternion.identity;
    }

    private void OnGuitar()
    {
        guitarStarted = true;

        if (!idleMode)
            AutoSwingOn();
    }


    //private void OnBeatStarted(float barInterval, int beatsPerBar)
    //{
    //	beatStarted = true;

    //	if (idleMode)
    //		AutoSwingOn();
    //}

    private float RollZAngle(float increment, bool leftTurn, bool limit = true)
    {
        rollZAngle = rollRotation.z + (leftTurn ? turnAngle : -turnAngle) * increment;

        if (limit)
        {
            if (rollZAngle > maxTurnAngle)
                rollZAngle = maxTurnAngle;
            else if (rollZAngle < -maxTurnAngle)
                rollZAngle = -maxTurnAngle;
        }

        return rollZAngle;
    }

    //private IEnumerator ResetCountdown()
    //{
    //	Debug.Log("ResetCountdown");

    //	yield return new WaitForSeconds(outroTime);

    //	Debug.Log("OnTripRestart");
    //	TripEvents.OnTripRestart?.Invoke();

    //	yield return null;
    //}

    private void OnMusicOutro()
    {
        inOutro = true;

        if (!idleMode)
            AutoSwingOn();

        //StartCoroutine(ResetCountdown());		// timer to restart event
    }


    //private void ClampToCameraView()
    //{
    //    Vector3 clampPos = Camera.main.WorldToViewportPoint(transform.position);
    //    clampPos.x = Mathf.Clamp01(clampPos.x);
    //    clampPos.y = Mathf.Clamp01(clampPos.y);
    //    transform.position = Camera.main.ViewportToWorldPoint(clampPos);
    //}
}
