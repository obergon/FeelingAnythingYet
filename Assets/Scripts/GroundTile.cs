﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTile : MonoBehaviour
{
	public List<Transform> objectLayers = new List<Transform>();

    public Transform tileObject;        // plane / collider
    
	private int paintLayerIndex = 0;
	private bool chillTrip;

	public Transform CurrentPaintLayer { get { return objectLayers[paintLayerIndex]; } }
	
	private void OnEnable()
	{       
        TripEvents.OnTripStart += OnTripStart;    
        TripEvents.OnSwitchPaintLayer += OnSwitchPaintLayer;    
	}

	private void OnDisable()
	{
        TripEvents.OnTripStart -= OnTripStart;    
        TripEvents.OnSwitchPaintLayer -= OnSwitchPaintLayer;          
    }
    

	public void ClearSpawned()
	{
		foreach (var layer in objectLayers)
		{
			foreach (Transform child in layer.transform)
			{
				Destroy(child.gameObject);
			}
		}

		// tile should have no children!
        foreach (Transform child in tileObject.transform)
        {
            Destroy(child.gameObject);
        }
    }
    
    
    private void OnTripStart(bool isChill)
    {
        chillTrip = isChill;
        paintLayerIndex = 0;
    }
        
    private void OnSwitchPaintLayer()
	{
		if (chillTrip)
			return;
			
		if (objectLayers.Count <= 1)
			return;

		CurrentPaintLayer.gameObject.SetActive(false);

		if (paintLayerIndex == objectLayers.Count - 1)
			paintLayerIndex = 0;
		else
			paintLayerIndex++;
			
		CurrentPaintLayer.gameObject.SetActive(true);
	}
}