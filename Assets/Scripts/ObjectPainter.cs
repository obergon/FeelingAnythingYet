﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;


[RequireComponent(typeof(HoboController))]
public class ObjectPainter : MonoBehaviour
{
	public ObjectGroupSO thingPrefabs;              // list of prefabs
	public GameObject bubblePrefab;

	[Header("Colour Palettes")] //Maybe we should use arrays when having so many of varibles;                          
	public ColourPaletteSO chillGroundDayPalette;
	public ColourPaletteSO chillGroundNightPalette;
	public ColourPaletteSO chillPathDayPalette;
	public ColourPaletteSO chillPathNightPalette;
	public ColourPaletteSO chillSkyDayPalette;
	public ColourPaletteSO chillSkyNightPalette;
	
	public ColourPaletteSO anxiousGroundDayPalette;
	public ColourPaletteSO anxiousGroundNightPalette;
	public ColourPaletteSO anxiousPathDayPalette;
	public ColourPaletteSO anxiousPathNightPalette;
	public ColourPaletteSO anxiousSkyDayPalette;
	public ColourPaletteSO anxiousSkyNightPalette;
	
	[Header("Material Palettes")]
	public MaterialPaletteSO chillMaterialPalette;
	public MaterialPaletteSO anxiousMaterialPalette;

	private Coroutine paintCoroutine;
	private bool painting;
	private bool waitingForBeat = true;

    private bool inOutro = false;
	
	[Space]
	public float paintInterval;
	public bool paintOnBeat = true;

	public float paintMinScale;
	public float paintMaxScale;
	public float skyScaleFactor;        // sky is a long way away...
	public float pathScaleFactor = 0.25f;       // smaller

	public float paintMinTime;
	public float paintMaxTime;

	public float skyMinFadeDelay;
	public float skyMaxFadeDelay;

	public float fadeTime;

	private float spawnOffsetY = 1f;
    private int shotgunCount = 6;              	// chill trip, intense mood, paint clumps

	public GameObject paintParticles;			// spell 'beam'
	public GameObject spawnParticles;			// ground dust
	public float paintParticleTime;

	public float pathWidth = 8f;                // different objects spawned in player path

	private bool isDaytime = true;
	private bool tripStarted = false;
	private bool chillTrip = true;
	
	private bool idleMode = false;
	
	// for auto-painting at random intervals
	// currently auto-paints on music beat event
	//private Coroutine autoPaintCoroutine;
	//private float autoPaintMinInterval = 0.2f;
	//private float autoPaintMaxInterval = 0.5f;
	
	private HoboController hoboController;
    private BeatConductor.MusicMood currentMood = BeatConductor.MusicMood.Normal;

    public bool FadingOut { get; private set; }


	private void Start()
	{
		hoboController = GetComponent<HoboController>();
		inOutro = false;
		tripStarted = false;
	}
	
	private void OnEnable()
    {
        TripEvents.OnSunrise += OnSunrise;
        TripEvents.OnSunset += OnSunset;
        TripEvents.OnPlayerIdle += OnPlayerIdle;
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnTripRestart += OnTripRestart;
        TripEvents.OnBeatStarted += OnBeatStarted;
        TripEvents.OnMusicHalfBeat += OnMusicHalfBeat;
        TripEvents.OnMusicQtrBeat += OnMusicQtrBeat;
        TripEvents.OnMusicOutro += OnMusicOutro;
        TripEvents.OnMusicMoodChange += OnMusicMoodChange;
    }


    private void OnDisable()
    {
        TripEvents.OnSunrise -= OnSunrise;
        TripEvents.OnSunset -= OnSunset;
        TripEvents.OnPlayerIdle -= OnPlayerIdle;
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnTripRestart -= OnTripRestart;
        TripEvents.OnBeatStarted -= OnBeatStarted;
        TripEvents.OnMusicHalfBeat -= OnMusicHalfBeat;
        TripEvents.OnMusicQtrBeat -= OnMusicQtrBeat;
        TripEvents.OnMusicOutro -= OnMusicOutro;
        TripEvents.OnMusicMoodChange -= OnMusicMoodChange;
	}


	private void Update()
	{
        if (inOutro)
            return;

		if (!tripStarted)
			return;

        if (Input.GetMouseButtonDown(0))
            StartPainting();
        if (Input.GetMouseButtonUp(0))
            StopPainting();
    }
    

    private IEnumerator StartPausePaint()
    {
        if (painting || ! waitingForBeat)
            yield break;

        painting = true;
        while (painting)
        {
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
			{
				CastPaintSpell(hit.point, hit.transform);
				TripEvents.OnPlayerInput?.Invoke();
			}

            yield return new WaitForSeconds(paintInterval);
        }
        yield return null;
    }


    private void CastPaintSpell(Vector3 spawnPoint, Transform parentObject, int spellCount = 1)
    {
		if (parentObject.CompareTag("Ground"))
		{
			var groundSpawnPoint =  new Vector3(spawnPoint.x, spawnPoint.y + spawnOffsetY, spawnPoint.z);

            //Debug.Log("Spawn point = " + Mathf.Abs(groundSpawnPoint.x) + " width = " + pathWidth / 2 + " on path = " + (Mathf.Abs(groundSpawnPoint.x) < (pathWidth / 2)));

            if (Mathf.Abs(groundSpawnPoint.x) < (pathWidth / 2))		// on player's path
			{
				var randomPrefab = thingPrefabs.RandomThingPrefab(isDaytime, false, true, chillTrip);
				
				if (randomPrefab != null)
					PaintObject(randomPrefab, groundSpawnPoint, pathScaleFactor, parentObject, GetMaterialPalette(), GetColourPalette(true, false), true, 0f, spellCount);
			}
			else
			{
				var randomPrefab = thingPrefabs.RandomThingPrefab(isDaytime, false, false, chillTrip);
		
				if (randomPrefab != null)
					PaintObject(randomPrefab, groundSpawnPoint, 1.0f, parentObject, GetMaterialPalette(), GetColourPalette(false, false), true, 0f, spellCount);
			}
		}
		else if (parentObject.CompareTag("Sky"))
		{
			float fadeDelay = Random.Range(skyMinFadeDelay, skyMaxFadeDelay);
			var randomPrefab = thingPrefabs.RandomThingPrefab(isDaytime, true, false, chillTrip);
			
			if (randomPrefab != null)
				PaintObject(randomPrefab, spawnPoint, skyScaleFactor, parentObject, GetMaterialPalette(), GetColourPalette(false, true), false, fadeDelay, spellCount);
		}
    }


	private void PaintObject(GameObject prefab, Vector3 spawnPoint, float scaleFactor, Transform hitObject,
										MaterialPaletteSO materialPalette, ColourPaletteSO colourPalette,
										bool onGround, float fadeDelay = 0f, int spellCount = 1)
	{
        for (int i = 0; i < spellCount; i++)
        {
            var newThing = Instantiate(prefab);

            newThing.transform.localScale = Vector3.zero;

            float paintScale = UnityEngine.Random.Range(paintMinScale, paintMaxScale) * scaleFactor;
            var groundSpawnPoint = new Vector3(spawnPoint.x, spawnPoint.y - 1, spawnPoint.z);
            newThing.transform.position = groundSpawnPoint;

            float paintTime = UnityEngine.Random.Range(paintMinTime, paintMaxTime);
            Color paintColour = Color.white;

            var materialController = newThing.GetComponent<MaterialController>();
            if (materialController != null)
            {
                paintColour = materialController.SetColourPalette(colourPalette);       // sets random init colour
                materialController.SetMaterialPalette(materialPalette);                 // sets random init material
            }

            LeanTween.scale(newThing, new Vector3(paintScale, paintScale, paintScale), paintTime)
                                        .setEase(LeanTweenType.easeInOutElastic)
                                        .setOnStart(() =>
                                        {
                                            newThing.transform.rotation = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);

                                            if (onGround)
                                            {
                                                var groundLayer = hitObject.parent.GetComponent<GroundTile>().CurrentPaintLayer;
                                                newThing.transform.SetParent(groundLayer);      // unscaled 
                                        	}
                                            else
                                            {
                                                var skyLayer = hitObject.parent.GetComponent<SkySphere>().CurrentPaintLayer;
                                                newThing.transform.SetParent(skyLayer);      // unscaled 
                                        	}
                                        })
                                        .setOnComplete(() =>
                                        {
                                            if (fadeDelay > 0)
                                                DelayFadeOut(newThing, fadeDelay);
                                        });


            if (i == 0 && paintParticles != null)
            {
                var particles = Instantiate(paintParticles);            // plays on awake
                particles.transform.position = hoboController.spellCastPoint.position;

                var main = particles.GetComponent<ParticleSystem>().main;
                main.startColor = paintColour;

                LeanTween.move(particles, spawnPoint, paintParticleTime)
                                        .setEase(LeanTweenType.easeInOutSine)
                                        .setOnComplete(() =>
                                        {
                                            Destroy(particles);
                                        });
            }
        }
		
		// dust particles on ground objects
		//if (onGround && spawnParticles != null)
		//{
		//	var particles = Instantiate(spawnParticles);			// plays on awake
		//	particles.transform.position = spawnPoint;

		//	var main = particles.GetComponent<ParticleSystem>().main; 
		//	main.startColor = paintColour;
		//}
    }

	// on mouse down
    private void StartPainting()
    {
		if (waitingForBeat)
		{
			StopPainting();
			paintCoroutine = StartCoroutine(StartPausePaint());
		}
		else
		{
			painting = true;

			// paint immediately on mouse down for instant feedback, on half beat thereafter
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
			{
				CastPaintSpell(hit.point, hit.transform);
				TripEvents.OnPlayerInput?.Invoke();
			}
		}
    }

	// on mouse up
    private void StopPainting()
    {
		painting = false;

		if (paintCoroutine != null)
		{
			StopCoroutine(paintCoroutine);
			paintCoroutine = null;
		}
    }
    

    private Vector3 RandomScreenPoint
	{
		get { return new Vector3(Random.Range(0f, (float)Screen.width),
									Random.Range(0f, (float)Screen.height), 0f); }
	}
	
	// superceded by paint on beat event
    
 	//private IEnumerator AutoPaint()
	//{
	//	while (idleMode)
	//	{
	//		//Debug.Log("AutoPaint");
	//		if (Physics.Raycast(Camera.main.ScreenPointToRay(RandomScreenPoint), out RaycastHit hit))
	//			CastPaintSpell(hit.point, hit.transform);

	//		yield return new WaitForSeconds(UnityEngine.Random.Range(autoPaintMinInterval, autoPaintMaxInterval));
	//	}

	//	yield return null;
	//}
    
	//private void AutoPaintOn()  
	//{
	//	AutoPaintOff();
	//	autoPaintCoroutine = StartCoroutine(AutoPaint());
	//}
	

	//private void AutoPaintOff()
	//{
	//	if (autoPaintCoroutine != null)
	//	{
	//		StopCoroutine(autoPaintCoroutine);
	//		autoPaintCoroutine = null;
	//	}
	//}

    private void OnTripStart(bool isChill)
    {
		tripStarted = true;
        chillTrip = isChill;
        waitingForBeat = true;
    }
    
    
	private void OnTripRestart()
	{
		tripStarted = false;
		inOutro = false;
	}
    

    private void OnMusicOutro()
    {
        StopPainting();
        inOutro = true;
    }

    private void OnPlayerIdle(bool isIdle, bool forceCut)
	{
		if (idleMode == isIdle)
			return;

		idleMode = isIdle;

		if (idleMode)
			StopPainting();
		
		// random auto-paint superceded by auto-paint on music beat
		//if (isIdle)
		//	AutoPaintOn();
		//else
			//AutoPaintOff();
	}

    private void OnMusicMoodChange(BeatConductor.MusicMood mood)
    {
        currentMood = mood;
    }

    private void PaintThing(int paintCount = 1)
    {
        if (idleMode || painting || inOutro)
        {
            var paintPoint = idleMode ? RandomScreenPoint : Input.mousePosition;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(paintPoint), out RaycastHit hit))
            {
                CastPaintSpell(hit.point, hit.transform, paintCount); 

                if (painting)
                    TripEvents.OnPlayerInput?.Invoke();
            }
        }
    }

    private void OnMusicQtrBeat(float barInterval, int beatsPerBar, int qtrBeatCount, float curveValue)
    {
        if (currentMood == BeatConductor.MusicMood.Intense)
        {
            if (chillTrip)
                PaintThing(shotgunCount);
            else
                PaintThing();
        }
        else if (currentMood == BeatConductor.MusicMood.Normal || inOutro)
        {
            PaintThing();
        }
    }

    // auto paint on half-beat
    private void OnMusicHalfBeat(float barInterval, int beatsPerBar, int halfBeatCount, float curveValue)
	{
		if (currentMood != BeatConductor.MusicMood.Normal)		// normal on qtr beat only
        	PaintThing();
    }
	
	private void OnBeatStarted(int beatCount, float barInterval, int beatsPerBar)
	{
		waitingForBeat = false;
		
		// stop painting - it's now timed with the beat
		if (paintCoroutine != null)
		{
			StopCoroutine(paintCoroutine);
			paintCoroutine = null;
		}
	}
	

	private void DelayFadeOut(GameObject newThing, float fadeDelay)
	{
		// fade out and destroy after fadeDelay
		var materialController = newThing.GetComponent<MaterialController>();

		if (fadeDelay > 0f && materialController != null)
		{
			LeanTween.value(newThing, materialController.LastColour, Color.clear, fadeTime)
						.setDelay(fadeDelay)
						.setEase(LeanTweenType.easeInCubic)
						.setOnStart(() =>
						{
							//FadingOut = true;
							TripEvents.OnObjectFadeOut?.Invoke(materialController);
						})
						.setOnUpdate((Color val) =>
						{
							materialController.SetColour(val);
						})
						.setOnComplete(() =>
						{
							//FadingOut = false;
							Destroy(newThing);
						});

			//LeanTween.value(newThing, 1f, 0f, fadeTime)
						//.setDelay(fadeDelay)
						//.setEase(LeanTweenType.easeInCubic)
						//.setOnStart(() =>
						//{
						//	//FadingOut = true;
      //                      TripEvents.OnObjectFadeOut?.Invoke(materialController);
						//})
						//.setOnUpdate((float val) =>
						//{
						//	materialController.SetTransparency(val);
						//})
						//.setOnComplete(() =>
						//{
						//	//FadingOut = false;
						//	Destroy(newThing);
						//});
		}
	}
	
	
	private ColourPaletteSO GetColourPalette(bool onPath, bool onSky)
	{
		if (chillTrip)
		{
			if (isDaytime)
				return onPath ? chillPathDayPalette :
									(onSky ? chillSkyDayPalette :
									(onPath ? chillPathDayPalette : chillGroundDayPalette));
			else
				return onPath ? chillPathNightPalette :
									(onSky ? chillSkyNightPalette :
									(onPath ? chillPathNightPalette : chillGroundNightPalette));
		}
		else
		{
			if (isDaytime)
				return onPath ? anxiousPathDayPalette :
									(onSky ? anxiousSkyDayPalette :
									(onPath ? anxiousPathDayPalette : anxiousGroundDayPalette));
			else
				return onPath ? anxiousPathNightPalette :
									(onSky ? anxiousSkyNightPalette :
									(onPath ? anxiousPathNightPalette : anxiousGroundNightPalette));
		}
	}

	private MaterialPaletteSO GetMaterialPalette()
	{
		return chillTrip ? chillMaterialPalette : anxiousMaterialPalette;
	}
		
	private void OnSunset()
	{
		Debug.Log("Sunset");
		isDaytime = false;
	}

	private void OnSunrise()
	{
		Debug.Log("Sunrise");
		isDaytime = true;
	}
}
