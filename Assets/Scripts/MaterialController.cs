﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MaterialEffects))]
public class MaterialController : MonoBehaviour
{
    public ColourPaletteSO colourPalette { get; private set; }
    public MaterialPaletteSO materialPalette { get; private set; }
    
	private LeanTweenType colourChangeEasing = LeanTweenType.easeInOutSine;
	
    private MeshRenderer objectsMeshRender;
    public Color LastColour { get; private set; }
    private int lastColourIndex = 0;
    private int lastMaterialIndex = 0;
    
    private MaterialEffects materialEffects;

    private LTDescr colourTweenId;

	//private bool fadingOut;

    private void Awake()
    {
        materialEffects = GetComponent<MaterialEffects>();
        objectsMeshRender = GetComponent<MeshRenderer>();
        
		SetColour(Color.clear);
    }

    private void OnEnable()
    {
        TripEvents.OnMusicBeat += ChangeMaterialColour;
        TripEvents.OnObjectFadeOut += OnFadeOut;
    }

    private void OnDisable()
    {
        TripEvents.OnMusicBeat -= ChangeMaterialColour;
        TripEvents.OnObjectFadeOut -= OnFadeOut;
    }


	private void ChangeMaterialColour(float barInterval, int beatsPerBar, int beatCount, float curveValue)
	{
		ChangeColour(barInterval, beatsPerBar);
		
		//if (beatCount == beatsPerBar)		// on every bar
			//ChangeMaterial();
	}
	
    private void ChangeColour(float barInterval, int beatsPerBar)
    {
		//if (fadingOut)
		//	return;

		if (colourPalette == null)
			return;
			
        var randomColourIndex = 0;

        while ((randomColourIndex = UnityEngine.Random.Range(0, colourPalette.colorGroup.Length)) == lastColourIndex)
        {
            // keep trying if random index is same as last 
        }
        
        var randomColour = colourPalette.colorGroup[randomColourIndex];
        lastColourIndex = randomColourIndex;

        colourTweenId = LeanTween.value(gameObject, LastColour, randomColour, barInterval / beatsPerBar)
        		.setEase(colourChangeEasing)
                .setOnStart(() =>
                {
                    LastColour = randomColour;
                })
                .setOnUpdate((Color val) =>
                {
                	//if (! fadingOut)
                    	materialEffects.UpdateColour(val);
                });
    }

    private void OnFadeOut(MaterialController fadingObject)
    {
		//Debug.Log("OnFadeOut " + gameObject.GetInstanceID() + " " + fadingObject.gameObject.GetInstanceID());
		if (colourTweenId != null && gameObject.GetInstanceID() == fadingObject.gameObject.GetInstanceID())
		{
			//Debug.Log("OnFadeOut cancel tween " + colourTweenId.id);
			LeanTween.cancel(colourTweenId.id);
		}
		
		//fadingOut = true;
    }

	public void SetColour(Color colour)
	{
		LastColour = colour;
		materialEffects.UpdateColour(colour);
	}

	public void SetTransparency(float val)
	{
		materialEffects.UpdateTransparency(val);
	}

	// returns random init colour
	public Color SetColourPalette(ColourPaletteSO palette)
	{
		//Debug.Log("SetColourPalette: palette = " + palette.name);
			
		colourPalette = palette;
		SetColour(palette.RandomColour);

		return LastColour;
	}
	
	// returns random init material
	public Material SetMaterialPalette(MaterialPaletteSO palette)
	{
		materialPalette = palette;
		var randomMat = palette.RandomMaterial;
		SetAllMaterials(randomMat);
		
		//Debug.Log("SetMaterialPalette: randomMat = " + randomMat.name);

		return randomMat;
	}

    private void ChangeMaterial()
    {
		if (materialPalette == null)
			return;
			
        var randomMatIndex = 0;

        while ((randomMatIndex = UnityEngine.Random.Range(0, materialPalette.materialGroup.Length)) == lastMaterialIndex)
        {
            // keep trying if random index is same as last 
        }

		var randomMat = materialPalette.materialGroup[randomMatIndex];
        lastMaterialIndex = randomMatIndex;

		SetAllMaterials(randomMat);
    }
    
   
	private void SetAllMaterials(Material newMat)
	{
		 var allRenderers = GetComponentsInChildren<Renderer>();
		 
		 foreach (Renderer rend in allRenderers)
		 {
		     var mats = new Material[rend.materials.Length];
		     
		     for (var m = 0; m < rend.materials.Length; m++)
		     {
		         mats[m] = newMat;
		     }
		     
		     rend.materials = mats;
		 }
	}
}
