﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class itemButton : MonoBehaviour
{
    public UnityEvent switchScene;
    public UnityEvent fadeOut;
    public UnityEvent fadeIn;

    private void OnMouseDown()
    {
        //Debug.Log("Ive been clicked");
        Invoke("FadeOut", 0);
        Invoke("FadeIn", 4f);
        Invoke("SwitchScene", 0.5f);
    }
    void FadeOut()
    {
        //Debug.Log("Fading Out");
        fadeOut.Invoke();
    }
    
    void FadeIn()
    {
        //Debug.Log("Fading In");
        fadeIn.Invoke();
    }
    
    void SwitchScene()
    {
        //Debug.Log("Switching Camera");
        switchScene.Invoke();
    }
}
