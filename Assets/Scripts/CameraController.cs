﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class CameraController : MonoBehaviour
{
	public CinemachineClearShot idleCamera;
    public GameObject menuObjects;          // hands, etc

    private Animator stateCameraAnimator;
    
    //private bool waitingForTrip = false;
    private bool inOutro = false;
	private bool isIdle = false;


    private void Awake()
    {
        stateCameraAnimator = GetComponent<Animator>();
        //stateCameraAnimator.enabled = false;
    }

    private void Start()
    {
        ActivateFPVCam();
    }

    private void OnEnable()
    {
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnPlayerIdle += OnPlayerIdle;
        TripEvents.OnBeatStarted += OnBeatStarted;
        TripEvents.OnMusicOutro += OnMusicOutro;
        TripEvents.OnTripRestart += OnRestart;
    }

    private void OnDisable()
    {
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnPlayerIdle -= OnPlayerIdle;
        TripEvents.OnBeatStarted -= OnBeatStarted;
        TripEvents.OnMusicOutro -= OnMusicOutro;
        TripEvents.OnTripRestart -= OnRestart;
    }


    private void Update()
    {
        if (inOutro)
            return;

        // TODO: trip selection not working, so reinstated this temporarily..
        /*	if (Input.GetMouseButtonDown(0) && inMenu)      // TODO: on selection of left/right hand
            {
                TripEvents.OnTripStart?.Invoke(true);       // TODO: false if anxiety trip	
                ActivatePlayingCam();
            } */
    }

    public void StartAnxious()
    {
        TripEvents.OnTripStart?.Invoke(false);
        ActivatePlayingCam();
    }

    public void StartChill()
    {
        TripEvents.OnTripStart?.Invoke(true);
        ActivatePlayingCam();
    }

    private void ActivatePlayingCam()
    {
        stateCameraAnimator.SetTrigger("Playing");
        //waitingForTrip = false;
    }

    private void ActivateFPVCam()
    {
        //Debug.Log("ActivateFPVCam");
        stateCameraAnimator.SetTrigger("Menu");
        //waitingForTrip = true;
        inOutro = false;
        menuObjects.SetActive(true);		// show hands 
    }

    private void ActivateIdleCam()
    {
        stateCameraAnimator.SetTrigger("Idle");			// clearshot virtual cameras - random selection
        //waitingForTrip = false;			// just in case!
    }

    private void ActivateOutroCam()
    {
        //Debug.Log("ActivateOutroCam");
        stateCameraAnimator.SetTrigger("Outro");
        //waitingForTrip = false;         // just in case!
        inOutro = true;
    }


    private void OnTripStart(bool isChill)
    {
        //stateCameraAnimator.enabled = true;     // start timeline (events)
    }

    private void OnPlayerIdle(bool idle, bool forceCut)
    {
		if (idle && forceCut)
		{
			idleCamera.ResetRandomization();
		}
		else if (isIdle != idle)
		{
			isIdle = idle;

			if (isIdle)
				ActivateIdleCam();
			else
				ActivatePlayingCam();

			menuObjects.SetActive(false);       // hide hands 
		}
    }

    private void OnMusicOutro()
    {
        ActivateOutroCam();
    }

    private void OnBeatStarted(int beatCount, float barInterval, int beatsPerBar)
    {
        menuObjects.SetActive(false);       // hide hands 
    }

    private void OnRestart()
    {
        Debug.Log("CameraController.OnRestart");
		inOutro = false;
        ActivateFPVCam();
    }
}
