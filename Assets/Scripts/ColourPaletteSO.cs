﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Colour Palette")]
public class ColourPaletteSO : ScriptableObject
{
    public Color[] colorGroup;
    
    public Color RandomColour { get { return colorGroup [ Random.Range(0, colorGroup.Length) ]; } }
}
