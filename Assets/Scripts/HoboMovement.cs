﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoboMovement : MonoBehaviour
{
	public float bounceHeight;
	public LeanTweenType bounceTween = LeanTweenType.easeOutSine;
	public AudioClip bounceAudio;

	public ParticleSystem dust;

    public float normalMoodHeightFactor = 1f;
    public float manicMoodHeightFactor = 2f;
    public float quietMoodHeightFactor = 0.5f;

    private float moveHeight;

	private bool bounceHobo = false;


    private void Start()
    {
        moveHeight = bounceHeight;
    }

    private void OnEnable()
	{
		TripEvents.OnMusicBeat += OnBeat;
        TripEvents.OnMusicMoodChange += OnMusicMoodChange;
        TripEvents.OnTripStart += OnTripStart;
        TripEvents.OnTripRestart += OnTripRestart;
	}


	private void OnDisable()
	{
		TripEvents.OnMusicBeat -= OnBeat;
        TripEvents.OnMusicMoodChange -= OnMusicMoodChange;
        TripEvents.OnTripStart -= OnTripStart;
        TripEvents.OnTripRestart -= OnTripRestart;
    }
	
	
	// hobo bounces on every bar
	private void OnBeat(float barInterval, int beatsPerBar, int beatCount, float curveValue)
	{
		if (!bounceHobo)
			return;
			
		if (beatCount == beatsPerBar)
		{
			//Debug.Log("OnBar");
			var bouncePosition = new Vector3(transform.localPosition.x, transform.localPosition.y + moveHeight, transform.localPosition.z);

			LeanTween.moveLocal(gameObject, bouncePosition, barInterval / 2)     // 2 for up and back down again
									.setEase(bounceTween)
									.setOnComplete(() =>
									{
										//dust.Play();
										//AudioSource.PlayClipAtPoint(bounceAudio, transform.position, 0.25f);
									})
									.setLoopPingPong(1);                        // back
		}
	}


    private void OnMusicMoodChange(BeatConductor.MusicMood mood)
    {
        switch (mood)
        {
            case BeatConductor.MusicMood.Normal:
                moveHeight = bounceHeight * normalMoodHeightFactor;
                break;

            case BeatConductor.MusicMood.Quiet:
                moveHeight = bounceHeight * quietMoodHeightFactor;
                break;

            case BeatConductor.MusicMood.Intense:
                moveHeight = bounceHeight * manicMoodHeightFactor;
                break;
        }
    }
    

	private void OnTripStart(bool isChill)
	{
		bounceHobo = true;
	}
	    
	private void OnTripRestart()
	{
		bounceHobo = false;
	}
}
